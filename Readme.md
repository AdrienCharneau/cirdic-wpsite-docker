# Set up project


- Choose a '*wpbackup*' folder that contains a wordpress '*wp-content*' folder and a '*wordpress.sql*' database in it


- Go to project root folder ( '*cirdic-wpsite-docker*' ) and place the '*wpbackup*' folder in it


- Initialize project with '*docker-compose*'

> docker-compose up -d


- Go to any web browser (preferably using incognito mode) and go to '*phpMyAdmin*'

> http://localhost:8080


- Wait for the mysql database to load, and login as root (password: *password*)

- Select the '*wordpress*' database and go to the '*import*' tab

- Here, select the '*wpbackup/wordpress.sql*' file, import it and wait for a while

- If you're trying to rebuild the site from an *OVH* backup follow the steps explained in the '*Migrating from server*' section below

- Follow these commands to replace the 'wp-content' folder with your backup version

> docker-compose stop

> sudo rm -r wpfolder/wp-content && sudo cp -R wpbackup/wp-content wpfolder && sudo chown -R www-data:www-data wpfolder/wp-content

> docker-compose start


- Go to the main wordpress page in your web browser

> http://localhost:8001/wp-login.php

> http://localhost:8001/wp-admin




# Delete project


- Temporary delete the project without removing its volumes and folders (re-initialize with '*docker-compose up -d*')

	> docker-compose down


- Erase everything including volumes and folders

	> docker-compose down --volumes && sudo rm -r wpfolder wpbackup




# Migrating from server


**Only do this if you're building from an '*OVH* backup' version**


- In *phpMyAdmin*'s *wordpress* database, go to the '*SQL*' tab and execute two sql queries by copy-pasting their content into the box

> sql-queries/"1 - UPDATE options.sql"

> sql-queries/"2 - UPDATE posts.sql"


- (Optional) In the '*wp_users*' table, change the 'admin7434' password to something else (remember to select '*MD5*' in the dropdown '*Function*' menu to encrypt it)




# Migrating back to server


- Open the '*wpfolder/wp-config.php*' file in a text editor

- Remove these sections:

```
// a helper function to lookup "env_FILE", "env", then fallback
if (!function_exists('getenv_docker')) {
	// https://github.com/docker-library/wordpress/issues/588 (WP-CLI will load this file 2x)
	function getenv_docker($env, $default) {
		if ($fileEnv = getenv($env . '_FILE')) {
			return rtrim(file_get_contents($fileEnv), "\r\n");
		}
		else if (($val = getenv($env)) !== false) {
			return $val;
		}
		else {
			return $default;
		}
	}
}
```

```
// If we're behind a proxy server and using HTTPS, we need to alert WordPress of that fact
// see also https://wordpress.org/support/article/administration-over-ssl/#using-a-reverse-proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false) {
	$_SERVER['HTTPS'] = 'on';
}
// (we include this by default because reverse proxying is extremely common in container environments)

if ($configExtra = getenv_docker('WORDPRESS_CONFIG_EXTRA', '')) {
	eval($configExtra);
}
```


- Edit all the environment variables with the correct information. For '*DB_CHARSET*', '*DB_COLLATE*' and the authentication keys, just remove the '*getenv_docker()*' functions

- Go to *OVH*'s *phpMyAdmin*

- Delete all the tables found in the *wordpress* database

- Delete all the files and folders found in *OVH*'s '*www*' directory throught FTP 

- Import the new '*wpbackup/wordpress.sql*' file into the *wordpress* database and wait for a while

- Execute the two SQL queries (but interchange its urls from 'localhost:8000' to 'https://www.cirdic.fr' and 'https://www.cirdic.fr' to 'localhost:8000')

> sql-queries/"1 - UPDATE options.sql"

> sql-queries/"2 - UPDATE posts.sql"


- Add '*wpfolder/*' folder's contents into the 'www' directory throught FTP


- Go to the website's url in your web browser




# Update PHP version

https://www.ovh.com/manager/#/web/hosting/cirdic.fr




# Configure/Install/Delete OVH's Wordpress module

https://www.ovh.com/manager/#/web/hosting/cirdic.fr/module