UPDATE wp_options
SET option_value = replace(option_value, 'https://www.cirdic.fr', 'http://localhost:8001')
WHERE option_name = 'home'
OR option_name = 'siteurl';